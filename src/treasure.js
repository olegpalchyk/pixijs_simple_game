"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("./constants");
var pixi_js_1 = require("pixi.js");
var TreasureSprite = /** @class */ (function (_super) {
    __extends(TreasureSprite, _super);
    function TreasureSprite(img, windowWidth, windowHeight) {
        var _this = _super.call(this, img) || this;
        _this.width = 28;
        _this.height = 24;
        _this.position = {
            x: 0,
            y: 0
        };
        _this.position.x = windowWidth - constants_1.borderWidth - _this.width;
        _this.position.y = windowHeight / 2 - _this.height / 2;
        return _this;
    }
    TreasureSprite.prototype.getFullItemCharacteristics = function () {
        return {
            x: this.position.x,
            y: this.position.y,
            width: this.width,
            height: this.height
        };
    };
    return TreasureSprite;
}(pixi_js_1.Sprite));
exports.default = TreasureSprite;
//# sourceMappingURL=treasure.js.map