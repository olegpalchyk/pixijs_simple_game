"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DungeonSprite = /** @class */ (function () {
    function DungeonSprite(sprite, userWidth, userHeight) {
        this.sprite = sprite;
        this.width = userWidth;
        this.height = userHeight;
    }
    DungeonSprite.prototype.resize = function (width, height) {
        this.width = width;
        this.height = height;
    };
    return DungeonSprite;
}());
exports.default = DungeonSprite;
//# sourceMappingURL=dungeon.js.map