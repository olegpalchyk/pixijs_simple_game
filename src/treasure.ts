import {borderWidth} from './constants';
import {Sprite} from 'pixi.js';

class TreasureSprite extends Sprite{
    width = 28;
    height = 24;
    position ={
        x : 0,
        y : 0
    };
    constructor( img ,windowWidth : number, windowHeight : number){
        super(img);
        this.position.x = windowWidth - borderWidth - this.width;
        this.position.y = windowHeight/2 - this.height/2;
    }


    getFullItemCharacteristics(){
        return {
            x : this.position.x,
            y : this.position.y,
            width: this.width,
            height : this.height
        }
    }
}

export default TreasureSprite;
