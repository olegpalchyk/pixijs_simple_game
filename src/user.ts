import {borderWidth, screenWidth, screenHeight} from './constants';
import {Sprite} from 'pixi.js';

enum Direction {
    Up = 38,
    Down = 40,
    Left = 37,
    Right = 39
}
class UserSprite extends Sprite{
    width;
    height;
    position = {
        x : 0,
        y : 0
    }
    userSpeed : number = 5;
        constructor(img){
            super(img);
        this.width = 21;
        this.height = 32;
        this.position.x = borderWidth ;
        this.position.y = borderWidth ;
    }

    move(keyCode : number):void{
        switch (keyCode){
            case Direction.Down :{
                if((this.position.y + this.height/2 + this.userSpeed + borderWidth*2) <= screenHeight){
                    this.position.y +=this.userSpeed;
                }else{
                    this.position.y = screenHeight - borderWidth  - this.height;
                }
                break;
            }
            case Direction.Up :{
                if((this.position.y - this.userSpeed - borderWidth) >= 0){
                    this.position.y -=this.userSpeed;
                }else{
                    this.position.y = borderWidth;
                }
                break;
            }
            case Direction.Left : {
                if((this.position.x -this.width/2 - this.userSpeed - borderWidth ) >=0){
                    this.position.x -= this.userSpeed;

                }else{
                    this.position.x = borderWidth + this.width/4;
                }
                break;
            }
            case Direction.Right :{
                if((this.position.x + this.userSpeed +this.width+ borderWidth) >= screenWidth){
                    this.position.x = screenWidth - borderWidth - this.width*3/4;
                }else{
                    this.position.x +=this.userSpeed;
                }
                break;
            }
        }
    }
    getFullItemCharacteristics(){
        return {
            x : this.position.x,
            y : this.position.y,
            width: this.width,
            height : this.height
        }
    }



}

export default UserSprite;
