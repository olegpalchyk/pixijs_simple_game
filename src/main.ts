import './style.css';
import * as PIXI  from 'pixi.js';
import {Sprite, Text} from 'pixi.js';
import UserSprite from './user';
import TreasureSprite from './treasure';
import DungeonSprite from './dungeon';

//screen sizes
import {screenHeight as windowHeight, screenWidth as windowWidth} from './constants'


//UserSprite obj main params;
let userWidth : number = 40;
let userHeigth : number = 40;
//
class Game {
    app;
    stage;
    dungeon;
    user;
    treasure;
    state;
    isTreasureGot :boolean = false;
    gotTreasureSprite;

    constructor(){
        this.app = PIXI.autoDetectRenderer(windowHeight,windowWidth, {antialias: false, transparent: false, resolution: 1});
        this.app.view.style.position = "absolute";
        this.app.view.style.display = "block";
        document.body.appendChild(this.app.view);
        this.stage = new PIXI.Container();
        PIXI.loader.add('./img/main.json')
            .load(() : void=>{
               this.initSprites();

            });

    }
    initSprites(){
        this.createDungeon();
        this.putTreasure();
        this.createUser();
        this.addListeners();
        this.state = this.play;
        this.gameLoop();

    }
    play(){
        if(!this.isTreasureGot){
            if(this.userOnTreasure()){
                this.userGotTreasure();
            }
        }
    }
    userGotTreasure(){
        this.isTreasureGot = true;
        this.stage.removeChild(this.treasure);
        this.gotTreasureSprite =   new Text(
            "You got treasure!",
            {fontFamily: "Futura", fill: "white", fontSize : '64px'}
        );
        this.gotTreasureSprite.x = 60;
        this.gotTreasureSprite.y = this.stage.height / 2 - 32;
        this.stage.addChild(this.gotTreasureSprite);
        setTimeout(()=>{
            this.stage.removeChild(this.gotTreasureSprite);
        }, 2000)

    }

    userOnTreasure(){
        let treasureCharacteristic = this.treasure.getFullItemCharacteristics();
        let userCharacteristic = this.user.getFullItemCharacteristics();
        let centrPointOfUser = {
            x : userCharacteristic.x + userCharacteristic.width/2,
            y : userCharacteristic.y + userCharacteristic.height/2
        };
        let inxideTreasureX = treasureCharacteristic.x < centrPointOfUser.x && treasureCharacteristic.x + treasureCharacteristic.width >centrPointOfUser.x;
        let inxideTreasureY = treasureCharacteristic.y < centrPointOfUser.y && treasureCharacteristic.y + treasureCharacteristic.height >centrPointOfUser.y;

        return inxideTreasureX && inxideTreasureY;

    }

    addListeners(){
        window.addEventListener('keydown', (event)=>{
            this.moveUser(event.keyCode)
        });
    }
    createDungeon(){
          let sprite = new Sprite(PIXI.utils.TextureCache['dungeon.png']);
          this.dungeon = new DungeonSprite(sprite, windowWidth, windowHeight);
          this.stage.addChild(sprite);
    }

    createUser(){
          this.user = new UserSprite(PIXI.utils.TextureCache['explorer.png']);
          this.stage.addChild(this.user);
    }
    moveUser(keyCode){
        // window.requestAnimationFrame(this.moveUser);
        this.user.move(keyCode);
    }
    putTreasure(){
        this.treasure = new TreasureSprite(PIXI.utils.TextureCache['treasure.png'], windowWidth , windowHeight);
        this.stage.addChild(this.treasure);
    }

    gameLoop(){
        //Loop this function 60 times per second
        requestAnimationFrame(this.gameLoop.bind(this));
        //Update the current game state
        this.state();
        //Render the stage
        this.app.render(this.stage);
    }


}



let game = new Game();




