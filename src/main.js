"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("./style.css");
var PIXI = require("pixi.js");
var pixi_js_1 = require("pixi.js");
var user_1 = require("./user");
var treasure_1 = require("./treasure");
var dungeon_1 = require("./dungeon");
//screen sizes
var constants_1 = require("./constants");
//UserSprite obj main params;
var userWidth = 40;
var userHeigth = 40;
//
var Game = /** @class */ (function () {
    function Game() {
        var _this = this;
        this.isTreasureGot = false;
        this.app = PIXI.autoDetectRenderer(constants_1.screenHeight, constants_1.screenWidth, { antialias: false, transparent: false, resolution: 1 });
        this.app.view.style.position = "absolute";
        this.app.view.style.display = "block";
        document.body.appendChild(this.app.view);
        this.stage = new PIXI.Container();
        PIXI.loader.add('./img/main.json')
            .load(function () {
            _this.initSprites();
        });
    }
    Game.prototype.initSprites = function () {
        this.createDungeon();
        this.putTreasure();
        this.createUser();
        this.addListeners();
        this.state = this.play;
        this.gameLoop();
    };
    Game.prototype.play = function () {
        if (!this.isTreasureGot) {
            if (this.userOnTreasure()) {
                this.userGotTreasure();
            }
        }
    };
    Game.prototype.userGotTreasure = function () {
        var _this = this;
        this.isTreasureGot = true;
        this.stage.removeChild(this.treasure);
        this.gotTreasureSprite = new pixi_js_1.Text("You got treasure!", { fontFamily: "Futura", fill: "white", fontSize: '64px' });
        this.gotTreasureSprite.x = 60;
        this.gotTreasureSprite.y = this.stage.height / 2 - 32;
        this.stage.addChild(this.gotTreasureSprite);
        setTimeout(function () {
            _this.stage.removeChild(_this.gotTreasureSprite);
        }, 2000);
    };
    Game.prototype.userOnTreasure = function () {
        var treasureCharacteristic = this.treasure.getFullItemCharacteristics();
        var userCharacteristic = this.user.getFullItemCharacteristics();
        var centrPointOfUser = {
            x: userCharacteristic.x + userCharacteristic.width / 2,
            y: userCharacteristic.y + userCharacteristic.height / 2
        };
        var inxideTreasureX = treasureCharacteristic.x < centrPointOfUser.x && treasureCharacteristic.x + treasureCharacteristic.width > centrPointOfUser.x;
        var inxideTreasureY = treasureCharacteristic.y < centrPointOfUser.y && treasureCharacteristic.y + treasureCharacteristic.height > centrPointOfUser.y;
        return inxideTreasureX && inxideTreasureY;
    };
    Game.prototype.addListeners = function () {
        var _this = this;
        window.addEventListener('keydown', function (event) {
            _this.moveUser(event.keyCode);
        });
    };
    Game.prototype.createDungeon = function () {
        var sprite = new pixi_js_1.Sprite(PIXI.utils.TextureCache['dungeon.png']);
        this.dungeon = new dungeon_1.default(sprite, constants_1.screenWidth, constants_1.screenHeight);
        this.stage.addChild(sprite);
    };
    Game.prototype.createUser = function () {
        this.user = new user_1.default(PIXI.utils.TextureCache['explorer.png']);
        this.stage.addChild(this.user);
    };
    Game.prototype.moveUser = function (keyCode) {
        // window.requestAnimationFrame(this.moveUser);
        this.user.move(keyCode);
    };
    Game.prototype.putTreasure = function () {
        this.treasure = new treasure_1.default(PIXI.utils.TextureCache['treasure.png'], constants_1.screenWidth, constants_1.screenHeight);
        this.stage.addChild(this.treasure);
    };
    Game.prototype.gameLoop = function () {
        //Loop this function 60 times per second
        requestAnimationFrame(this.gameLoop.bind(this));
        //Update the current game state
        this.state();
        //Render the stage
        this.app.render(this.stage);
    };
    return Game;
}());
var game = new Game();
//# sourceMappingURL=main.js.map