
class DungeonSprite {
    sprite;
    width;
    height;
    constructor(sprite, userWidth : number, userHeight : number){
        this.sprite = sprite;
        this.width = userWidth;
        this.height = userHeight;

    }

    resize(width : number, height : number){
        this.width = width;
        this.height = height
    }
}

export default DungeonSprite;
