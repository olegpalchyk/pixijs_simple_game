"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("./constants");
var pixi_js_1 = require("pixi.js");
var Direction;
(function (Direction) {
    Direction[Direction["Up"] = 38] = "Up";
    Direction[Direction["Down"] = 40] = "Down";
    Direction[Direction["Left"] = 37] = "Left";
    Direction[Direction["Right"] = 39] = "Right";
})(Direction || (Direction = {}));
var UserSprite = /** @class */ (function (_super) {
    __extends(UserSprite, _super);
    function UserSprite(img) {
        var _this = _super.call(this, img) || this;
        _this.position = {
            x: 0,
            y: 0
        };
        _this.userSpeed = 5;
        _this.width = 21;
        _this.height = 32;
        _this.position.x = constants_1.borderWidth;
        _this.position.y = constants_1.borderWidth;
        return _this;
    }
    UserSprite.prototype.move = function (keyCode) {
        switch (keyCode) {
            case Direction.Down: {
                if ((this.position.y + this.height / 2 + this.userSpeed + constants_1.borderWidth * 2) <= constants_1.screenHeight) {
                    this.position.y += this.userSpeed;
                }
                else {
                    this.position.y = constants_1.screenHeight - constants_1.borderWidth - this.height;
                }
                break;
            }
            case Direction.Up: {
                if ((this.position.y - this.userSpeed - constants_1.borderWidth) >= 0) {
                    this.position.y -= this.userSpeed;
                }
                else {
                    this.position.y = constants_1.borderWidth;
                }
                break;
            }
            case Direction.Left: {
                if ((this.position.x - this.width / 2 - this.userSpeed - constants_1.borderWidth) >= 0) {
                    this.position.x -= this.userSpeed;
                }
                else {
                    this.position.x = constants_1.borderWidth + this.width / 4;
                }
                break;
            }
            case Direction.Right: {
                if ((this.position.x + this.userSpeed + this.width + constants_1.borderWidth) >= constants_1.screenWidth) {
                    this.position.x = constants_1.screenWidth - constants_1.borderWidth - this.width * 3 / 4;
                }
                else {
                    this.position.x += this.userSpeed;
                }
                break;
            }
        }
    };
    UserSprite.prototype.getFullItemCharacteristics = function () {
        return {
            x: this.position.x,
            y: this.position.y,
            width: this.width,
            height: this.height
        };
    };
    return UserSprite;
}(pixi_js_1.Sprite));
exports.default = UserSprite;
//# sourceMappingURL=user.js.map